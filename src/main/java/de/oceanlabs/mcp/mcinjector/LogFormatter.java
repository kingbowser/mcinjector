package de.oceanlabs.mcp.mcinjector;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

class LogFormatter extends Formatter {

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    public synchronized String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        String message = this.formatMessage(record);
        sb.append(record.getLevel().getName());
        sb.append(": ");
        sb.append(message);
        sb.append("\n");
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            try (PrintWriter pw = new PrintWriter(sw)) {
                record.getThrown().printStackTrace(pw);
                sb.append(sw.toString());
            } catch (Exception ex) {
                // ignore
            }
        }
        return sb.toString();
    }

}
